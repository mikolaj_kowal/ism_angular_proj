import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { UserService } from './services/user.service';
import { FunctionCallService } from './services/function.call.service'
import { UserComponent } from './user/user.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { StatisticsFunctionsComponent } from './statistics-functions/statistics-functions.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UserRegisterComponent,
    StatisticsFunctionsComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    RouterModule.forRoot([
      {
        path: '',
        component: UserComponent
      },
      {
        path:'register',
        component: UserRegisterComponent
      },
      {
        path:'statistics/functions',
        component: StatisticsFunctionsComponent
      }
    ])
  ],
  providers: [UserService, FunctionCallService],
  bootstrap: [AppComponent]
})
export class AppModule { }
