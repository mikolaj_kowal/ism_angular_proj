import {UserModel} from "./user-model"

export class UserRegisterModel {
        // firstName:string;
        // lastName: string;
        // username: string;
        // password: string;
        // conf_password: string;
        // email: string;
        // conf_email: string;

    toUserModel(): UserModel{
        var toReturn = new UserModel();
        toReturn.email = this.email;
        toReturn.firstName = this.firstName;
        toReturn.password = this.password;
        toReturn.userAccessLevel = this.userAccessLevel;
        toReturn.lastName = this.lastName;
        toReturn.username = this.username;
        toReturn.id = this.id;
        return toReturn;
    }
        
    constructor(
        public firstName:string,
        public lastName: string,
        public username: string,
        public password: string,
        public conf_password: string,
        public email: string,
        public conf_email: string,
        public userAccessLevel:string="Normal",
        public id:number = 0
    ){

    }
}
