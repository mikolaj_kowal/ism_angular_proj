import { Component, OnInit } from '@angular/core';
import {UserRegisterModel} from './../assets/models/user-register-model'
import {UserService} from './../services/user.service';
import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent{

  constructor(private _userService: UserService, private _router: Router) { }

  // model = new UserRegisterModel('Mikolaj', 'Kowal', 'mkowal', '1234', '1234', 'cos@cos.com', 'cos@cos.com');
  model = new UserRegisterModel('', '', '', '', '', '', '');
  show_error = false;
  error_msg = '';

  onSubmit(){
      this.show_error = false;
      this._userService.addUser(this.model).subscribe(
        response =>  this.succes_callback(response),
        err=> this.error_callback(err)
      );
  }

  error_callback(response){
    console.log('error', response)
    this.show_error = true;
    this.error_msg = response.error.msg;
  }

  succes_callback(response){
    console.log('success', response)
    this._router.navigate(['/'])
  }

  get diagnostic() { return JSON.stringify(this.model); }

}
