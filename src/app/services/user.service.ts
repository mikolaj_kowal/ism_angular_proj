import { UserModel } from './../assets/models/user-model';
import {UserRegisterModel} from './../assets/models/user-register-model';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http'


@Injectable()
export class UserService {
  constructor(private _http: HttpClient) {}

  getUsers(): Observable<Array<UserModel>> {
    return this._http.get<Array<any>>('http://localhost:8080/user/findByAccessLevel?level=Admin,Normal,Moderator');
  }

  addUser(user: UserRegisterModel): Observable<any> {
    var toAdd = user.toUserModel();
    console.log(toAdd);
    return this._http.post<any>('http://localhost:8080/user/', toAdd);
  }

  updateUser(toUpdate: UserModel): Observable<any>{
    return this._http.put<any>('http://localhost:8080/user/', toUpdate)
  }

  removeUser(deleteId): Observable<any>{
    return this._http.delete<any>("http://localhost:8080/user/"+deleteId);
  }
}