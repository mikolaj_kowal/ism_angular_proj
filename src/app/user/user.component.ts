import { sample } from 'rxjs/operator/sample';
import { UserService } from './../services/user.service';
import { UserModel } from './../assets/models/user-model';
import { Component, OnInit, ViewChild } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserRegisterModel} from "../assets/models/user-register-model"
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  usersArray: Array<UserModel> = new Array<UserModel>();
  clickedUser = null;
  currentModal = null;

  show_error = false;
  error_msg = "";

  userTypes = ['Normal', 'Moderator', 'Admin'];

  constructor(private _userService: UserService, private modalService: NgbModal, private _router: Router) { }

  ngOnInit() {
    this.usersArray = new Array<UserModel>();
    this.getUsers();
  }

  getUsers(){
    this._userService.getUsers().subscribe(user=>{
      user.forEach(item=>{
        this.usersArray.push(item);
      })
    })
  }

  editUser(id, modal){
    this.clickedUser = this.toRegisterModel(this.usersArray.find(user=>user.id == id));
    this.currentModal = this.modalService.open(modal);
  }

  register(){
    this._router.navigate(["/register"]);
  }

  toRegisterModel(userModel): UserRegisterModel{
    var toReturn = new UserRegisterModel(userModel.firstName,
                                         userModel.lastName,
                                         userModel.username,
                                         '',
                                         '',
                                         userModel.email,
                                         userModel.email,
                                         userModel.userAccessLevel,
                                         userModel.id);
    return toReturn;
}

submitUser(){
  var toUpdate = this.clickedUser.toUserModel();
  console.log(toUpdate)
  this._userService.updateUser(toUpdate).subscribe(
    response =>  this.update_success_callback(response),
    err=> this.update_error_callback(err)
  );

}

update_error_callback(response){
  console.log('error', response)
    this.show_error = true;
    this.error_msg = response.error.msg;
}

update_success_callback(response){
  this.closeModal();
  this.ngOnInit();
}

delete(){
  this._userService.removeUser(this.clickedUser).subscribe(
    response =>  {this.ngOnInit(); this.closeModal()},
    err=> {}
  );
}

closeModal(){
  this.currentModal.close();
}

deleteUserModal(id, modal){
  this.clickedUser = id;
  this.currentModal = this.modalService.open(modal);
  
}

}
